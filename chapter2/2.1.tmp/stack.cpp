#include "stack.h"

namespace Stack{	//data expression
	const int max_size = 20;
	chat v[max_size];
	int top = -1;
}

void Stack::push(char c){
	v[++top] = c;
}

char Stack::pop(){
	return v[top--];
}
