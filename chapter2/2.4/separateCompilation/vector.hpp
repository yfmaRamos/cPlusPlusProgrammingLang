#include <iostream>
using namespace std;


class Vector
{
	public:
		Vector(int s);	//constructor
		double & operator[](int i);	//operator []
		int getSize();	//get the size of this vector
	private:
		int size;
		double * elem;
}

/*
 * Above is a interface of the vector which only contains
 * member declarations.
 */

