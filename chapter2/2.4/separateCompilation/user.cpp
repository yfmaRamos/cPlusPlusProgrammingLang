#include "vector.hpp"
#include <iostream>
#include <cmath>
using namespace std;

double sqrt_sum(const Vector& v)
{
	double sum=0;
	for (int i=0;i<v.getSize();i++)
		sum += sqrt(v[i]);
	return sum;
}
