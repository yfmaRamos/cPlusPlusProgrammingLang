#include <iostream>
using namespace std;


class Vector
{
	public:
		Vector(int s);	//constructor
		double & operator[](int i);	//operator []
		int getSize();	//get the size of this vector
	private:
		int size;
		double * elem;
}

/*
 * Above is a interface of the vector which only contains
 * member declarations.
 */

Vector::Vector(int s):size(s),elem(New double[s]){} //constructor definition

double & operator[](int i)
{
	if (i<0 && i>=size){
		cout<<"Wrong index! Cannot find the answer!"<<endl;
		break;
	}else{
		return elem[i];	//by default this way return a true double.
	}
}

int Vector::getSize()
{
	return size;
}
