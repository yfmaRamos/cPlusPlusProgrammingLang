#include <iostream>

enum class TrafficLight{green,yellow,red};	//enum class

TrafficLight light=TrafficLight::red;	//red,yellow,green are all in the scope of the enum class TrafficLight

/*
 * Note:
 * enum class has only assignments, initialization and comparisons by default.
 * However, this class is actually user defined which means opearations can be edited ont it.
 */

TrafficLight & operator++(TrafficLight & t)
{
	switch(t){
		case TrafficLight::green: return t=TrafficLight::yellow;
		case TrafficLight::yellow: return  t=TrafficLight::red;
		case TrafficLight::red:	return t=TrafficLight::green;
	}
}

TrafficLight next = ++light;	//light becomes green,next becomes green

